namespace Model.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class OnlineHelpDbContext : DbContext
    {
        public OnlineHelpDbContext()
            : base("name=OnlineHelp")
        {
        }

        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Belonging> Belongings { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Facility> Facilities { get; set; }
        public virtual DbSet<Request> Requests { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Request>()
                .Property(e => e.RequestDateTime)
                .IsFixedLength();
        }
    }
}
