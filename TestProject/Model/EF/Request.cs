namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Request")]
    public partial class Request
    {
        public long RequestID { get; set; }

        [StringLength(50)]
        public string Requestor { get; set; }

        public long? FacilityID { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] RequestDateTime { get; set; }

        public int? Status { get; set; }

        public int? Type { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        public long? UserID { get; set; }

        public long? EmployeeID { get; set; }
    }
}
