namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Belonging
    {
        public long ID { get; set; }

        public long? RoomID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
    }
}
